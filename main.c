/*
 * File:   main.c
 * Author: rance
 *
 * Created on September 25, 2020, 8:09 AM
 */

#include "mcc_generated_files/system.h"
#include "mcc_generated_files/uart1.h"
#include "mcc_generated_files/interrupt_manager.h"
#include <stdio.h>
#include <stdint.h>
#include "mcc_generated_files/clock.h"
#include <libpic30.h>
#include "AM2302.h"
#include "UART.h"

/*
                         Main application
 */

int main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    INTERRUPT_GlobalEnable();
    UART1_SetRxInterruptHandler(rxHandler);
    
    printf("Hello from PIC\n");
    
    while (1)
    {
        if (getPollFlag()) {
            pollSensor();
            resetPollFlag();
        }  
    }

    return 1;
}

/**
 End of File
*/

