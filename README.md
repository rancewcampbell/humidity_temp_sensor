# Humidity & Temperature Sensor

This is code made for my Microchip PIC24FJ256GA7 Development Board and an Asair AM2302 Humidity and Temperature Sensor. The UART code was generated using Microchip Code Configurator (MCC). _AM2302.c_ contains the necessary code to interpret the data from the AM2302. In order to work properly the data pin needs to be configured in the MCC Pin Module. _UART.c_ contains the code for polling the sensor. Any terminal will work for this that has RS232 communication capabilities. I use [Termite](https://www.compuphase.com/software_termite.htm) and it works great for this application. The character necessary to poll the sensor is defined in UART.c. I chose **'0'** but this could be any character. To make this communication possible, I used the USB UART Click breakout board by MikroE since my dev board has headers for these specific breakout boards. I'm sure most USB to UART interfaces would work well.

_Do not forget the necessary resistor that needs to be connected between power and the communication pin. This is talked about in the AM2302 instructions found online [here](https://cdn-shop.adafruit.com/datasheets/Digital+humidity+and+temperature+sensor+AM2302.pdf)._

![terminal screenshot](docs/screenshots/terminal_screenshot.png 'Screenshot of Terminal Window')
![pic/humidity sensor setup](docs/screenshots/humidity_sensor_pic_setup.jpg 'Picture of my PIC dev board with sensor attache')

## Dependancies

- MPLAB X IDE

- XC16 Compiler (or equivalent for your PIC)

- Microchip Code Configurator (MCC)

- PIC Microcontroller [Microchip PIC24FJ256GA7 Development Board](https://www.microchip.com/DevelopmentTools/ProductDetails/DM240016) is the one I used, but should port to most PICs that have MCC capability

- [Asair AM2302 Humidity and Temperature Sensor](https://www.adafruit.com/product/393)

- RS232 communication terminal

- [USB UART Click Breakout Board](https://www.mikroe.com/usb-uart-click)
