/*
 * File:   UART.h
 * Author: rance
 *
 * Created on October 20, 2020, 8:09 AM
 */

#include <xc.h> // include processor files - each processor file is guarded.  

void pollSensor(void);

void setPollFlag(void);

void resetPollFlag(void);

bool getPollFlag(void);

void rxHandler(void);
