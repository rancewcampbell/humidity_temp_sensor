/*
 * File:   AM2302.h
 * Author: rance
 *
 * Created on September 25, 2020, 8:09 AM
 */

#include <xc.h> // include processor files - each processor file is guarded.
#include "mcc_generated_files/clock.h"

#define FCY (CLOCK_PeripheralFrequencyGet())
#include <libpic30.h>

typedef struct {
    uint16_t rh;
    uint16_t temp;
    uint16_t checksum;
} AM2302_data;

void startRead(void);

short checkResponse(void);

AM2302_data readData(void);

void readBit(uint16_t* data);