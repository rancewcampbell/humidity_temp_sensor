/*
 * File:   UART.c
 * Author: rance
 *
 * Created on October 20, 2020, 8:09 AM
 */

#include <stdbool.h>
#include <stdio.h>
#include "mcc_generated_files/uart1.h"
#include "AM2302.h"
#include "UART.h"

#define CFG_FAHRENHEIT 1
#define TEMP_POLL_CHAR 48

bool pollFlag = false;

void pollSensor(void) {
    AM2302_data temp_data;
    double temp_float;
        
    // begin a read
    startRead();

    // if AM2302 has a response, save response in temp_data
    if (checkResponse()) {
        temp_data = readData();
    }
    
    // print response to stdout
    
    // if CFG_FAHRENHEIT print temp in degrees F, else print temp in degrees C
#if defined(CFG_FAHRENHEIT)
    temp_float = ((double)temp_data.temp * 9 / 50) + 32;
#else
    temp_float = (double)temp / 10;
#endif
    printf("humidity = %.1f%%\n", (double)temp_data.rh / 10);
#if defined(CFG_FAHRENHEIT)
    printf("temp     = %.1f F\n", temp_float);
#else
    printf("temp     = %.1f C\n", temp_float);
#endif
    printf("checksum = %d\n", temp_data.checksum);

    __delay_us(100);
}

void setPollFlag(void) {
    pollFlag = true;
}

void resetPollFlag(void) {
    pollFlag = false;
}

bool getPollFlag(void) {
    return pollFlag;
}

// UART interrupt callback function
void rxHandler(void) {
    uint8_t data;
    if (UART1_IsRxReady()) {
        data = UART1_Read();
    }
    if (data == TEMP_POLL_CHAR) {
        setPollFlag();
    }
}
