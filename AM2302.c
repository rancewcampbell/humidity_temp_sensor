/*
 * File:   AM2302.c
 * Author: rance
 *
 * Created on September 25, 2020, 8:09 AM
 */

// Step 1:

// Host pulls low for 1~10ms minimum (output)

// Host pulls up and waits for 20~40us (input)

// Sensor pulls MCU low for 80us then pull up for 80us

// Step 2:

// Bit transmission starts w/ low voltage level for 50us

/*
 * After low voltage, high-voltage-level signal length decides 1 or 0
 * 
 *            High for 26-28us = 0
 * __            _______________
 *   \          /               \
 *    \        /                 \ 
 *     \______/                   \_____
 *     
 *  Start transmit bit
 * 
 * 
 *                        High for 70us = 1
 * __            _________________________________
 *   \          /                                 \
 *    \        /                                   \ 
 *     \______/                                     \_____
 *     
 *  Start transmit bit
 */


#include "AM2302.h"
#include "mcc_generated_files/pin_manager.h"
#include <stdbool.h>
#include <stdio.h>

#define DATA_LEN 40

// Send the start transmit signal.
void startRead(void) { 
    AM2302_DATA_SetDigitalOutput();
    AM2302_DATA_SetLow();
    __delay_ms(25);
    AM2302_DATA_SetHigh();
    __delay_us(30);
    AM2302_DATA_SetDigitalInput();
}

// Listen for AM2302 response
short checkResponse(void) {  
    short isResponse = 0;
    __delay_us(50);
    if (!AM2302_DATA_GetValue()) {
        __delay_us(50);
        if (AM2302_DATA_GetValue()) {
            __delay_us(50);
            isResponse = 1;
        }
    }
    return isResponse;
}

// Read incoming signal from AM2302
AM2302_data readData(void) {
    uint8_t i;
    AM2302_data temp_data = {0x0, 0x0, 0x0};
    for (i = 0; i < DATA_LEN; i++) {
        if (i <= 15) {
            readBit(&temp_data.rh);
        } else if (i <= 31) {
            readBit(&temp_data.temp);
        } else {
            readBit(&temp_data.checksum);
        }
        
    }
    return temp_data;
}

void readBit(uint16_t* data) {
    *data <<= 1;
    while (!AM2302_DATA_GetValue()) {
        ;
    }
    __delay_us(30);
    if (AM2302_DATA_GetValue()) {
        *data |= 0x1;
    }        
    while (AM2302_DATA_GetValue()) {
        ;
    }
}